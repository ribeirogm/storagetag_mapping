import json
import requests

BASE_URL = 'http://<Bamboo BASE URL>'
AUTH_TOKEN ='<Authentication token>'

GET_PLANS = '/rest/api/latest/search/plans'
GET_STORAGE = '/rest/api/latest/planDirectoryInfo/'
HEADERS = {"Authorization": "Bearer " + AUTH_TOKEN, 'Content-type': 'application/json', 'Accept': 'application/json'}

def get_number_of_plans():

	r = requests.get(BASE_URL + GET_PLANS, headers=HEADERS)
	plans = r.json()
	return plans['size']

def get_plans():

	max_results = get_number_of_plans()
	payload = {'expand': 'results.result', 'max-result': max_results}
	r = requests.get(BASE_URL + GET_PLANS, headers=HEADERS, params=payload)
	plans = json.loads(r.text) 
	return plans['searchResults']

def get_directory_info(plan_key, info):

    	r = requests.get(BASE_URL + GET_STORAGE + plan_key, headers=HEADERS)
    	storage = json.loads(r.text)
    	return storage['results'][0][info]

def print_data(plans):
	
	for plan in plans:
    	
		plan_key = (plan['id'])
		storage_tag = get_directory_info(plan_key,'storageTag')
		print ('Plan: ' + plan_key + ' Storage Tag: ' + storage_tag)

if __name__ == '__main__': 
    print_data(get_plans()) 



