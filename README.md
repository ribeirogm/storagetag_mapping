# PlanKey vs StorageTag mapping

This is simple Script that uses Bamboo REST API to map the PlanKey with its respective StorageTag.

This might be useful if you want to investigate disk space issue caused by the bamboo-home directory.


## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requests.

```bash
pip install requests
```

## Usage

* Make sure [Plan directory information REST API](https://confluence.atlassian.com/bamboo/plan-directory-information-rest-api-750396160.html) is enabled in Bamboo

* Set the BASE_URL and [AUTH_TOKEN](https://confluence.atlassian.com/bamboo/personal-access-tokens-976779873.html) variables inside the planinfo.py file:

```python
BASE_URL = 'http://<Bamboo BASE URL>'
AUTH_TOKEN ='<Authentication token>'
```

* Run the planinfo.py file

```bash
python planinfo.py
```

